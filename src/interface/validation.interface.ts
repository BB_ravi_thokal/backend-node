import { IsEmail, IsNotEmpty, IsPhoneNumber, IsMobilePhone, isMobilePhone } from 'class-validator'

// tslint:disable-next-line:max-classes-per-file
export class User {
    @IsNotEmpty()
    name: string

    @IsNotEmpty()
    password: string

    contactNumber: string

    @IsEmail()
    emailId: string

}

// tslint:disable-next-line:max-classes-per-file
export class UpdateUser {
    _id?: string
    @IsNotEmpty()
    name: string
    contactNumber: string

    @IsEmail()
    emailId: string
}

// tslint:disable-next-line:max-classes-per-file
export class Login {
    @IsNotEmpty()
    userId: string

    @IsNotEmpty()
    password: string
}

// tslint:disable-next-line:max-classes-per-file
export class ForgotPassword {
    @IsEmail()
    emailId: string
}

// tslint:disable-next-line:max-classes-per-file
export class ChangePassword {
    @IsEmail()
    emailId: string

    @IsNotEmpty()
    temporaryPassword: string

    @IsNotEmpty()
    newPassword: string
}