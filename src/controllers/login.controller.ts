import { Body, Get, JsonController, Post, UnauthorizedError, InternalServerError, QueryParam, NotFoundError, Put  } from 'routing-controllers';
import { Login, ForgotPassword, ChangePassword } from '../interface/validation.interface';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';
import Auth from '../models/auth.model';
import { Types } from "mongoose";
const { ObjectId } = Types;

@JsonController('/login')
export class LoginController {
    constructor(
        private authService: AuthService,
        private userService: UserService,
    ) {}

    @Post('/')
    async login(@Body() req: Login) {
        const data = await this.authService.login(req);
        if (data) {
            const user = await this.userService.getUser(undefined, data.authId);
            return {
                token: data.token,
                userData: user
            };
        } else {
            throw new UnauthorizedError('Failed to login. Please confirm user id and password');
        }
    }

    @Post('/change-password')
    async changePassword(@Body() req: ChangePassword) {
        const createTempPass = this.authService.changePassword(req);
        if (createTempPass) {
            return 'Temporary password sent on your email'
        } else {
            throw new NotFoundError('Entered email is not registered with us');
        }
    }

}