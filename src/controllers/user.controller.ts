import { BadRequestError, Body, Get, JsonController, Post, UnauthorizedError, InternalServerError, QueryParam, NotFoundError, Put, Authorized  } from 'routing-controllers';
import { UserService } from '../services/user.service';
import { Utils } from '../helpers/utils';
import { SharedService } from '../services/shared.service';
import { AuthService } from '../services/auth.service';
import { User, UpdateUser } from '../interface/validation.interface';

@JsonController('/user')
export class UserController {
    constructor(
        private userService: UserService,
        private sharedService: SharedService,
        private authService: AuthService,
    ) { }

    @Post('/create')
    async createUser(@Body() req: User) {
        const isUserExist = await this.sharedService.checkUserExist(req.emailId);
        if (isUserExist) {
            throw new UnauthorizedError('Email Id already exist');
        } else {
            const encryptPassword = await Utils.encryptPassword(req.password);
            const saveAuth = await this.authService.createAuth(req.emailId, encryptPassword);
            if (saveAuth) {
                const createUser = await this.userService.createUser(req, saveAuth._id);
                if (createUser) {
                    return JSON.parse(JSON.stringify({
                        message: 'User created successfully',
                        data: createUser
                    }));
                }
            } else {
                throw new InternalServerError('Not able to create user due to server error');
            }
        }
    }

    @Get('/')
    @Authorized()
    async getUser(@QueryParam('userId')id: string) {
        const user = await this.userService.getUser(id);
        if (user) {
            if (user) {
                return {
                    message: 'User fetch successfully',
                    data: JSON.parse(JSON.stringify(user))
                }
            } else {
                throw new NotFoundError('User not found');
            }
        }
    }

    @Put('/update')
    @Authorized()
    async updateUser(@Body() req: UpdateUser) {
        const user = await this.userService.updateUser(req);
        if (user) {
            return {
                message: 'User updated Successfully',
                data: JSON.parse(JSON.stringify(user))
            }
        } else {
            throw new NotFoundError('User not found');
        }
    }

    // @Put('/delete')
    // @Authorized()
    // async deleteUser(@Body() req: UpdateUser) {
    //     const authData = {
    //         _id: req.auth,
    //         isDeleted: true
    //     }
    //     const deleteAuth = await this.authService.deleteAuth(authData);
    //     if (deleteAuth) {
    //         const user = await this.userService.deleteUser(req);
    //         if (user) {
    //             return {
    //                 message: 'User deleted Successfully',
    //                 data: JSON.parse(JSON.stringify(user))
    //             }
    //         } else {
    //             throw new NotFoundError('User not found');
    //         }
    //     } else {
    //         throw new InternalServerError('Not able to remove user now please try later');
    //     }
    // }

}