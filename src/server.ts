import expressApp from "./app";

// tslint:disable-next-line:no-empty
expressApp.listen(process.env.PORT || 4000, () => {
    // tslint:disable-next-line:no-console
    console.log('Server started on port 4000 .....');

});
