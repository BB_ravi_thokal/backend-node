import Auth from '../models/auth.model';
import * as mongoose from 'mongoose';
import { Types } from "mongoose";
import { Utils } from '../helpers/utils';
import { UnauthorizedError, NotFoundError, InternalServerError } from 'routing-controllers';
const { ObjectId } = Types;

export class AuthService {
    async createAuth(emailId: string, pass: string): Promise<any> {
        const auth = new Auth({
            _id: new mongoose.Types.ObjectId(),
            userId: emailId,
            password: pass
        })
        const authRes = await auth.save();
        if (authRes) {
            return authRes;
        } else {
            return false;
        }
    }

    async deleteAuth(data: any): Promise<any> {
        if (data._id) {
            const auth = await Auth.updateOne({ _id: ObjectId(data._id) }, data);
            if (auth.nModified > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    async getAuthByUserId(id: string): Promise<any> {
        if (id) {
            const auth = await Auth.findOne({ userId: id, isDeleted: false });
            if (auth) {
                return auth;
            } else {
                return false;
            }
        }
    }

    async login(data: any): Promise<any> {
        const getAuth: any = await Auth.findOne({ userId: data.userId, isDeleted: false })
        if (getAuth) {
            const checkPassword = await Utils.compareDecryptedKey(data.password, getAuth.password);
            if (checkPassword) {
                const payload = {
                    userId: data.userId
                }
                const getToken = Utils.createJWTToken(payload);
                return {
                    authId: getAuth._id,
                    token: getToken,
                };
            } else {
                throw new UnauthorizedError('Please enter correct password');
            }
        } else {
            throw new NotFoundError('The mentioned user id does not exist');
        }
    }

    async changePassword(data: any) {
        const checkUser = await this.getAuthByUserId(data.emailId);
        if (checkUser) {
            const encryptPassword = await Utils.encryptPassword(data.newPassword);
            const auth = await Auth.updateOne({ _id: ObjectId(checkUser._id) }, { temporaryPassword: '', password: encryptPassword });
            if (auth.nModified > 0) {
                return true;
            } else {
                throw new InternalServerError('Not able to change the password. Please try again later');
            }
        } else {
            return false;
        }
    }
 }