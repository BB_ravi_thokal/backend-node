import User from '../models/user.model';
import { Types } from "mongoose";
const { ObjectId } = Types;
import * as mongoose from 'mongoose';

export class UserService {
    async createUser(data: any, authId: string): Promise<any> {
        const user = new User({
            _id: new mongoose.Types.ObjectId(),
            auth: authId,
            name: data.name,
            contactNumber: data.contactNumber,
            emailId: data.emailId
        });
        return await user.save().then(res => {
            if (res) {
                return res;
            } else {
                return false;
            }
        });
    }

    async getUser(id?: string, authId?: string): Promise<any> {
        if (id && authId) {
            const user = await User.findOne({_id: ObjectId(id), isDeleted: false, auth: ObjectId(authId)});
            return user;
        } else if (id) {
            const user = await User.findOne({_id: ObjectId(id), isDeleted: false});
            return user;
        } else if (authId) {
            const user = await User.findOne({auth: ObjectId(authId), isDeleted: false});
            return user;
        } else {
            const user = await User.find({ isDeleted: false });
            return user;
        }
    }

    async updateUser(data: any): Promise<any> {
        if (data._id) {
            // tslint:disable-next-line:no-console
            console.log('Data in update', data);
            const user = await User.updateOne({ _id: ObjectId(data._id) }, data);
            if (user.nModified > 0) {
                return true;
            } else {
                return false;
            }
        }
    }

    async deleteUser(data: any): Promise<any> {
        if (data._id) {
            const user = await User.updateOne({ _id: ObjectId(data._id) }, data);
            if (user.nModified > 0) {
                return true;
            } else {
                return false;
            }
        }
    }
}