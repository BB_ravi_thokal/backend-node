import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const authSchema = new Schema({
    _id: Schema.Types.ObjectId,
    userId: String,
    password: String,
    temporaryPassword: { type: String, default: '' },
    isDeleted: { type: Boolean,  default: false }
}, { timestamps: true });

export default mongoose.model('Auth', authSchema, 'Auth');