import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const userSchema = new Schema({
    _id: Schema.Types.ObjectId,
    auth: { type: Schema.Types.ObjectId, ref: 'Auth' },
    name: { type: String,  required: true },
    contactNumber: { type: String,  required: true },
    emailId: { type: String,  required: true },
    isDeleted: { type: Boolean,  default: false }
}, { timestamps: true });

export default mongoose.model('User', userSchema, 'User');