import { existsSync, mkdirSync } from 'fs';
import * as winston from 'winston';

const logDir: string = `./logs`;
const fileName = Date.now().toLocaleString;
if (!existsSync(logDir)) {
	mkdirSync(logDir);
}

export const logger = winston.createLogger({
	// level: 'error',
	format: winston.format.json(),
	transports: [
		// new winston.transports.Console(),
		new winston.transports.File({ filename: `${logDir}/${fileName}` }),
	]
});
