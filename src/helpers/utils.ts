import { sign, verify } from "jsonwebtoken";
import { JWT_SECRET, AES_SECRET } from "../config";
import { BadRequestError, UnauthorizedError } from "routing-controllers";
// import * as bcrypt from 'bcrypt';
// import { hashSync, compareSync } from 'bcrypt';
import { AES, enc } from 'crypto-ts';

export class Utils {

    public static createJWTToken(payload: any, options?: any) {
        try {
          const token = sign(payload, JWT_SECRET);
          return token;
        } catch (error) {
          throw new BadRequestError(error.message)
        }
    }

    static verifyToken(token: string) {
        try {
            const isVerified = verify(token, JWT_SECRET);
            return isVerified;
        } catch (error) {
            throw new UnauthorizedError(error.message)
        }
    }

    public static async encryptPassword(data: string) {
		try {
            const ciphertext = AES.encrypt(data, AES_SECRET).toString();
			return ciphertext;
		} catch (error) {
			throw new BadRequestError(error.message);
		}
	}

	public static async compareDecryptedKey(password: string, hash: string) {
		try {
            const bytes = AES.decrypt(hash, AES_SECRET);
            if (bytes.toString(enc.Utf8) === password) {
                return true;
            } else {
                return false;
            }
		} catch (error) {
			throw new BadRequestError(error.message);
		}
	}
}

