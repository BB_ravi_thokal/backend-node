import { Interceptor, InterceptorInterface, Action } from "routing-controllers";

@Interceptor()
export class ResponseInterceptor implements InterceptorInterface {

    intercept(action: Action, content: any) {
        const response = {
            success: true,
            data: JSON.parse(JSON.stringify(content)),
            message: 'Success'
        }
        return response;
    }

}